#!/bin/bash

# Add subcortical structures from freesurfer default parcellation into brodmann parcellation
# Generate connectivity matrix

path='/home/alba/00_WorkingData/2025_MDD/Patient2'
fs_LUT='/home/alba/00_WorkingData/2025_MDD/LUT/' #folder containing all the required LUT (I think the freesurfer one has
# changed with the new update. If using the new one (v7.3) the thalamus is no there anymore. I've copied the old one from
# the other computer)

cd $path

###### 1. Generate new LUT in /home/alba/00_WorkingData/2025_MDD/LUT/
# fs_default_subcortex.txt --> from fs_default only subcortical areas
# Brodmann_known_default_subcortex.txt --> new parcel (BA cerebra atlas + subcortex freesurfer)

###### 2. Generate new parcel subcortex freesurfer
mkdir 2patient/BA_subcortex_parcel

# Default freesurfer desikan parcellation
labelconvert DWI/reconAll/mri/aparc+aseg.mgz $fs_LUT/FreeSurferColorLUT.txt $fs_LUT/fs_default.txt \
 2patient/BA_subcortex_parcel/fs_parcel.mif -force

# Co-register to diffusion space
mrtransform 2patient/BA_subcortex_parcel/fs_parcel.mif \
-linear DWI/tractography/diff2struct.txt 2patient/BA_subcortex_parcel/fs_parcel_coreg.mif -inverse -force

# Get subcortical areas defined in fs_LUT/fs_default_subcortex.txt
labelconvert 2patient/BA_subcortex_parcel/fs_parcel_coreg.mif $fs_LUT/fs_default.txt $fs_LUT/fs_default_subcortex.txt \
 2patient/BA_subcortex_parcel/fs_parcel_subcortex.mif -force


###### 3. Merge 2 parcel images /cortical-Brodmann areas & subcortical-fs_default)
# regrid fs_parcel so image dimensions match
mrgrid 2patient/BA_subcortex_parcel/fs_parcel_subcortex.mif regrid -template 2patient/Brodmann_known_nodes_coreg.mif \
 2patient/BA_subcortex_parcel/fs_parcel_subcortex_regrid.mif -interp nearest -force

# subtract to avoid overlappings between BA25 and accumbens area
mrcalc  2patient/Brodmann_known_nodes_coreg.mif 2patient/BA_subcortex_parcel/fs_parcel_subcortex_regrid.mif -max - |\
 mrcalc - 2patient/Brodmann_known_nodes_coreg.mif -subtract 2patient/BA_subcortex_parcel/fs_parcel_subcortex_regrid_sub.mif -force

# merge both parcels together
mrcalc  2patient/Brodmann_known_nodes_coreg.mif 2patient/BA_subcortex_parcel/fs_parcel_subcortex_regrid_sub.mif -max \
 2patient/BA_subcortex_parcel/Brodmann_subcortex.mif -force


###### 4. Obtain connectivity files
# NAME_connectome_BAsubcortex.csv --> connectivity matrix (1st row/column correspond to non-assigned streamlines)
# IN/NAME_assignments_BAsubcortex.csv --> streamlines' assignments to start/end nodes
coreg_nodes='2patient/BA_subcortex_parcel/Brodmann_subcortex.mif' #modified BA parcellation + subcortex

for_each VTA_tracts/VTA* : tck2connectome IN/BOTH_tracks_NAME.tck $coreg_nodes IN/NAME_connectome_BAsubcortex.csv \
-tck_weights_in IN/Both_tracks_NAME_weights_GROUP.csv -out_assignments IN/NAME_assignments_BAsubcortex.csv \
-keep_unassigned -zero_diagonal -assignment_radial_search 4 -force
# if using radial search 2, less streamlines are assigned. I think increasing the value doesn't change the streamline
# assignment if the streamline is close to the node (it will always assign the streamline to the closest node).
# We are only more permissive with those streamlines that are not assigned.